import sqlite3
import sys
import pandas as pd
import logging

# задаём уровень логгирования
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
log = logging.getLogger('dbhelper')


def get_db_path():
    path = "db/rate2.db"  # f"{sys.path[0]}/db/rate2.db"
    # log.info(sys.path)
    return path


def get_con():
    """Получить соединение с БД"""
    db_path = get_db_path()
    con = sqlite3.connect(db_path)
    return con


def create_table(db_name):
    """Создать таблицу, если она не существует"""
    with get_con() as con:
        cur = con.cursor()
        cur.execute("PRAGMA  journal_mode=WAL")
        cur.execute(f"""
                CREATE TABLE IF NOT EXISTS '{db_name}'
                (dt INTEGER NOT NULL,  
                 price REAL, 
                 amount REAL, 
                 cost REAL,
                 side TEXT,
                 PRIMARY KEY (dt, price)
                 )
                """)
        con.commit()
        cur.close()


# sqlite3 context manager
class sql3open(object):
    """
    Simple CM for sqlite3 databases. Commits everything at exit.
    """

    def __init__(self, path=get_db_path()):
        self.path = path
        self.conn = None
        self.cursor = None

    def __enter__(self):
        self.conn = sqlite3.connect(self.path)
        self.cursor = self.conn.cursor()
        return self.cursor

    def __exit__(self, exc_class, exc, traceback):
        self.conn.commit()
        self.conn.close()


#
def get_db_data(tbl_name, startdate, enddate):
    startdate *= 1000
    enddate *= 1000
    # exch = tbl_name.split('::')[0]
    # sym = tbl_name.split('::')[1]
    df = None

    with get_con() as conn:
        sql = f"""
           select {startdate} as dt, NULL as price
           union 
           select dt, price from '{tbl_name}' where dt between {startdate} and {enddate}
           union
           select {enddate} as dt, NULL as price
           order by dt
           """
        df = pd.read_sql(con=conn, sql=sql, index_col='dt', parse_dates={'dt': 'ms'})

    # if exch == 'cccagg':
    #     df = get_data_cccagg(sym,startdate, enddate)
    # if exch == 'binance':
    #     df = get_data_binance(sym,startdate, enddate)
    # if exch == 'exmo':
    #     df = get_data_exmo(sym,startdate, enddate)
    # if exch == 'yobit':
    #     df = get_data_yobit(sym,startdate, enddate)

    return df

# def get_data_binance(sym, startdate, enddate):
#     with get_con() as conn:
#         sql = f"""
#         select {startdate} as dt, NULL as price
#         union
#         select dt, c as price from 'binance::{sym}' where dt >= {startdate}
#         union
#         select {enddate} as dt, NULL as price
#         order by dt
#         """
#         df = pd.read_sql(con=conn, sql=sql, index_col='dt', parse_dates={'dt': 's'})
#
#     return df
#
#
#
#
# def get_data_exmo(sym,startdate, enddate):
#     with get_con() as conn:
#         sql = f"""
#         select {startdate} as dt, NULL as price
#         union
#         select dt, price from 'exmo::{sym}' where dt >= {startdate}
#         union
#         select {enddate} as dt, NULL as price
#         order by dt
#         """
#         df = pd.read_sql(con=conn, sql=sql, index_col='dt', parse_dates={'dt': 's'})
#
#     return df
#
# def get_data_yobit(sym,startdate, enddate):
#     with get_con() as conn:
#         sql = f"""
#         select {startdate} as dt, NULL as price
#         union
#         select dt, price from 'yobit::{sym}' where dt >= {startdate}
#         union
#         select {enddate} as dt, NULL as price
#         order by dt
#         """
#         df = pd.read_sql(con=conn, sql=sql, index_col='dt', parse_dates={'dt': 's'})
#
#     return df
#
#
# def get_data_cccagg(sym,startdate, enddate):
#     startdate *=1000
#     enddate *=1000
#
#     with get_con() as conn:
#         sql = f"""
#         select {startdate} as dt, NULL as price
#         union
#         select dt, price from 'cccagg::{sym}' where dt >= {startdate}
#         union
#         select {enddate} as dt, NULL as price
#         order by dt
#         """
#         df = pd.read_sql(con=conn, sql=sql, index_col='dt', parse_dates={'dt': 'ms'})
#
#     return df
