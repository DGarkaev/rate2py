import re
import time
import sqlite3
import queue
import pandas as pd
import logging
import json
import os.path

# import datatable as dtbl
from utils import dbhelper

log = logging.getLogger('common')


def is_dev():
    """ разработка или продакшн"""
    # raise Exception('No abs path')
    return os.path.exists('../dev')

def tf2a(timeframe):
    """
    Парсим таймфрейм в массив
    :param timeframe:string таймфрейм из Pandas
    :return:array массив из 2-х элементов
    """
    tf = re.split('(\D+)', timeframe)
    return [int(tf[0]), tf[1]]

def tf2sec(timeframe):
    """
    Перевод таймфрейма в секунды
    :param timeframe:
    :return:
    """
    k = 60
    tf = tf2a(timeframe)
    if tf[1] == 'T':
        k=60
    if tf[1] == 'H':
        k=60*60
    if tf[1] == 'D':
        k=24*60*60
    limit = int(tf[0])*k
    return limit


def get_tblname(e, s):
    tbl_name = f"{e}::{s}"
    return tbl_name

def get_tblconvname(s):
    """
    Получим имя таблицы для конвертации
    :param s:
    :return:
    """
    return get_tblname('cccagg', s)

    # if s == "USD/RUB":
    #     return get_tblname('cccagg', s)
    # if s == "BTC/RUB":
    #     return get_tblname('cccagg', s)
    #
    # if s == "USDT/RUB" or s == "TUSD/RUB":
    #     return get_tblname('cccagg', s)


def get_rate(q: queue, tbl_name, startdate, enddate, tf):
    dt = time.monotonic()
    # startdate *= 1000
    # enddate *= 1000
    data_length = 0
    try:
        df1 = None

        df1 = dbhelper.get_db_data(tbl_name, startdate, enddate)


        df1['price'] = pd.to_numeric(df1['price'], errors='coerce')
        if len(df1) < 3:
            raise Exception(f"'{tbl_name}' response Null from select")
        data_length = len(df1)
        # print(f"{tbl_name}: len = {len(df1)}")
        df1 = df1.ffill()
        last_rate = df1['price'][-1]
        df1 = df1.resample(tf, label='right').mean().bfill()
        df1['price'][-1] = last_rate
    except Exception as e:
        log.error(e)
        df1 = None

    q.put(df1)
    dt = time.monotonic() - dt
    log.debug(f"table: '{tbl_name}': read {data_length} row and resample by {dt:.4f} s")


def get_diff(d1: pd.DataFrame, d2: pd.DataFrame) -> pd.DataFrame:
    dt = time.monotonic()
    df = (d2 - d1) / d1 * 100
    dt = time.monotonic() - dt
    log.info(f"get_diff: {dt} s")
    return df


def get_ma_diff(d, ma):
    dt = time.monotonic()
    ma_diff = d.rolling(window=ma).mean()
    dt = time.monotonic() - dt
    log.info(f"get_ma_diff: {dt} s")
    return ma_diff


# def get_conf():
#     """Загрузка конфигурации"""
#     with open("config/rate2.json", "r") as read_file:
#         conf = json.load(read_file)
#     return conf




# //коридор
# /**
#  * @param $d array отклонения
#  * @param $ma array скользящая средняя
#  * @param $n int период скользящей
#  * @return array
#  */
# function get_bounds($d, $ma, $n)
# {
#     $bound1 = [];
#     $bound2 = [];
#     foreach ($ma as $k => $v) {
#         $b1 = [];
#         $b2 = [];
#         for ($j = 0; $j < $n; $j++) {
#             $b = $d[$k - $j]['value'];
#             if ($b > $v['value'])
#                 $b1[] = $b;
#             else
#                 $b2[] = $b;
#         }
#         $bound1[$k]['date'] = $v['date'];
#         $bound1[$k]['value'] = count($b1) > 0 ? array_sum($b1) / count($b1) : null;
#         $bound2[$k]['date'] = $v['date'];
#         $bound2[$k]['value'] = array_sum($b2) / count($b2);
#     }
#     return array(
#         'bound1' => $bound1,
#         'bound2' => $bound2
#     );
# }
def get_bounds(diff, ma):
    return [pd.DataFrame(), pd.DataFrame()]


def as_float(obj):
    """Checks each dict passed to this function if it contains the key "value"
    Args:
        obj (dict): The object to decode

    Returns:
        dict: The new dictionary with changes if necessary
    """
    if "value" in obj:
        obj["value"] = float(obj["value"])
    return obj


def fix_floats(data):
    if isinstance(data, list):
        iterator = enumerate(data)
    elif isinstance(data, dict):
        iterator = data.items()
    else:
        raise TypeError("can only traverse list or dict")

    for i, value in iterator:
        if isinstance(value, (list, dict)):
            fix_floats(value)
        elif isinstance(value, str):
            try:
                data[i] = float(value)
            except ValueError:
                pass
