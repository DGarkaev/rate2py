import yaml
import os, sys


def get_config(configname):
    # print(sys.path)
    env = 'dev/' if os.path.exists('dev') else ''
    with open(f"{sys.path[0]}/config/{env}{configname}", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
    return cfg
