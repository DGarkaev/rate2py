#!/bin/bash

pip install --upgrade pip
pip install requests flask flask_cors pytz pandas rq uwsgi pyyaml
#  --threads 2
uwsgi   --http-socket 0.0.0.0:3213 --wsgi-file app_rate2.py \
        --callable app --processes 4 --stats 0.0.0.0:9191 \
        --master --enable-threads -b 65536 --harakiri 5