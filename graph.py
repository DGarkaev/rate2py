from utils import common
import time
from multiprocessing import Process, Queue
# import queue


def graph_data(req):
    tt = time.time()
    exch1 = req.args['exch1']
    curr1 = req.args['curr1']
    exch2 = req.args['exch2']
    curr2 = req.args['curr2']

    exch1 = exch1.lower()
    curr1 = curr1.upper()
    exch2 = exch2.lower()
    curr2 = curr2.upper()

    # получаем таймфрейм
    tf = req.args.get('tf', '15T')
    tf = tf.upper()

    count_data = int(req.args.get('limit', 100))

    ma = req.args.get('ma', None)
    if ma is None:
        raise Exception("Variable 'ma' is not define")

    # ma = ma.split(':')
    # ma_tf = ma[1]
    # ma = int(ma[0])
    ma = [int(m) for m in ma.split(',')]  # .lower()
    # рассчитаем limit по длине ma в зависимости от тайфрейма
    limit = (max([count_data, max(ma)]) + count_data) * common.tf2sec(tf)

    startdate = int(time.time()) - limit
    enddate = int(time.time())

    th = []

    q1 = Queue()
    q2 = Queue()
    tbl1 = common.get_tblname(exch1, curr1)
    tbl2 = common.get_tblname(exch2, curr2)
    th.append(Process(target=common.get_rate, args=(q1, tbl1, startdate, enddate, tf)))
    th.append(Process(target=common.get_rate, args=(q2, tbl2, startdate, enddate, tf)))

    # если базовая валюта первого символа != RUB - получим данные для конвертации
    base1 = curr1.split('/')[1]
    d3 = None
    if base1 != 'RUB':
        q3 = Queue()
        tbl3 = common.get_tblconvname(f"{base1}/RUB")
        th.append(Process(target=common.get_rate, args=(q3, tbl3, startdate, enddate, tf)))

    # если базовая валюта второго символа != RUB - получим данные для конвертации
    base2 = curr2.split('/')[1]
    d4 = None
    if base2 != 'RUB':
        q4 = Queue()
        tbl4 = common.get_tblconvname(f"{base2}/RUB")
        th.append(Process(target=common.get_rate, args=(q4, tbl4, startdate, enddate, tf)))

    for t in th:
        t.start()

    for t in th:
        t.join()

    d1 = q1.get()
    d2 = q2.get()

    if d1 is None:
        raise Exception(f"[{tbl1}] response Null")

    if d2 is None:
        raise Exception(f"[{tbl2}] response Null")

    # if d1 is None or d2 is None:
    #     return '{"Error": true, "msg": "data1 or data2 is None"}'

    if base1 != 'RUB':
        d3 = q3.get()
        if d3 is None:
            raise Exception(f"[{tbl3}] response Null")
        # конвертируем
        d1 = d1 * d3

    if base2 != 'RUB':
        d4 = q4.get()
        if d4 is None:
            raise Exception(f"[{tbl4}] response Null")

        # конвертируем
        d2 = d2 * d4

    #  вычислим отклонения между d1 и d2
    diff = common.get_diff(d1, d2)

    #  вычислим MA по отклонениям
    ma_diff = list()
    for m in ma:
        ma_diff.append(common.get_ma_diff(diff, m)['price'].tail(count_data))

    # вычислим коридоры
    bounds = common.get_bounds(diff, ma_diff[0])

    d1 = d1.tail(count_data)
    d2 = d2.tail(count_data)
    diff = diff.tail(count_data)

    d1 = d1['price'].to_json()
    d2 = d2['price'].to_json()
    diff = diff['price'].to_json()

    ma_diff_str = list()
    for m in ma_diff:
        ma_diff_str.append(m.to_json())

    r = '{"date":' + f"{enddate},\n" + \
        f'"data1":{d1},\n"data2":{d2},\n' + \
        f'"diff":{diff},\n' + \
        '"ma":[' + ",\n".join(ma_diff_str) + '],\n' + \
        '"bounds": [ null, null]'

    if d3 is not None:
        d3 = d3['price'].tail(count_data).to_json()
        r += f', "conv1":{d3}'
    if d4 is not None:
        d4 = d4['price'].tail(count_data).to_json()
        r += f', "conv2":{d4}'

    r += '}'
    tt = time.time() - tt
    # print(tt)
    return r
