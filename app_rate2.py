# аналитика для графиков
import os
import pickle
import graph
from flask import Flask, jsonify
from flask import request
import redis
from utils import confighelper

app = Flask(__name__)
app.config.from_object(__name__)

cfg = confighelper.get_config('rate2.yml')

rd = redis.StrictRedis(host=cfg['redis']['host'], port=cfg['redis'].get('port', 6379))


# получить список доступных бирж
@app.route('/exchanges')
def get_exchs():
    cfg = confighelper.get_config('graph.yml')
    exch = cfg["exchanges"]
    return jsonify(exch)


@app.route('/data')
def get_data():
    try:
        rz = graph.graph_data(request)
    except Exception as e:
        rz = jsonify({"result": False, "msg": f"{e}"})
    return rz


# @app.route('/cross_rub')
@app.route('/investing_rate')
def investing_rate():
    try:
        rz = {}
        key = cfg['investing']
        data = rd.hgetall(key)
        for k, v in data.items():
            rz[k.decode("utf-8")] = float(v)
    except Exception as e:
        rz = {"result": False, "msg": f"{e}"}
    return jsonify(rz)


@app.route('/binance_ticker')
def binance_tickers():
    try:
        symbol: str = request.args['s']
        symbol = symbol.upper()
        sym = symbol.split(',')
        rz = []
        for i in sym:
            d = rd.hgetall(f"binance:ticker:{i.replace('/', '')}")
            rz.append({'s': i, 'p': float(d[b'c'])})
    except Exception as e:
        rz = {"result": False, "msg": f"{e}"}
    return jsonify(rz)


if __name__ == '__main__':
    app.run()
    # http_server = WSGIServer(('', 3213), app)
    # http_server.environ['wsgi.multiprocess'] = True
    # http_server.environ['wsgi.multithread'] = True
    # http_server.serve_forever()
