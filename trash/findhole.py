import sqlite3
import pandas as pd
import math

tblname = "binance::BTC/USDT"

con = sqlite3.connect('rate2.db')
cur = con.cursor()

# получим минимально доступное время
sql = f'select min(dt) from `{tblname}`'
cur.execute(sql)
t0 = cur.fetchone()[0]
cur.close()

# 1566444698833 => August 22, 2019 3:31:38.833
# math.floor(1566444698833/3600000)*3600000 = 1566442800000
# 1566442800000 => August 22, 2019 3:00:00
t0 = math.floor(t0 / 3600000) * 3600000
t1 = t0 + (60 * 60 - 1) * 1000

sql = f"""
select {t0} as dt, null as price
union
select dt, 1 as price from `{tblname}` where dt between {t0} and {t1}
"""

df = pd.read_sql(sql=sql, con=con, index_col='dt', parse_dates={'dt': 'ms'})
df = df.resample('1T').mean()
print(df)
pass
