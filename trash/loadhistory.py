import sqlite3
import json
import ccxt
from datetime import datetime
import time
from threading import Thread
import os, sys, getopt


os.chdir(sys.path[0])


def load_history():
    print("Download and update DB")

    global mode
    mode = 'all'

    global del_history
    del_history = False

    mode = 'all'
    argv = sys.argv[1:]
    if len(argv):
        opts, args = getopt.getopt(argv, "hr:", ["range=", "delhistory"])
        for opt, arg in opts:
            if opt in ("-r", "--range"):
                mode = arg
            if opt in ("-d", "--delhistory"):
                del_history = True

    with open("rate2.json", "r") as read_file:
        conf = json.load(read_file)

    threads = []
    for e in conf['exchanges']:
        # не загружаем где источник Cryptocompare
        if conf['exchanges'][e].get('source', None) is None:
            th = (Thread(target=thread_rate,
                         name=e,
                         args=(conf, e,)))
            threads.append(th)
    #загрузим данные через cryptocompare
    # th = (Thread(target=cc_rate,
    #              name='cc',
    #              args=(conf,)))
    # threads.append(th)

    for t in threads:
        t.start()

    for t in threads:
        t.join()

#Загрузка котировок
def cc_rate(conf):
    con = sqlite3.connect("rate2.db")
    cur = con.cursor()
    cur.execute("PRAGMA  journal_mode=WAL")

    for e in conf['exchanges']:
        if conf['exchanges'][e].get('source', None) is not None:
            coins = conf['exchanges'][e]['pairs']
            for c in coins:
                print(c)
            # rate = cc.get_historical_price_minute(e, )



def thread_rate(conf, exch):
    global mode
    global del_history
    con = sqlite3.connect("rate2.db")
    cur = con.cursor()
    cur.execute("PRAGMA  journal_mode=WAL")

    nday = conf['history_days']
    # времяв БД хранится в мс
    starttime = int(time.time()) - nday * 24 * 60 * 60

    # количество котировок в одной секции
    limit = conf['exchanges'][exch].get('limit', 100)

    # get instance exchange
    if conf['exchanges'][exch].get('source', '') != 'cc':
        exchange = create_exchange(exch)

    # проходим по списку валютных пар
    for s in conf['exchanges'][exch]['pairs']:
        # print('-' * 80)
        db_name = f"{exch}::{s}"
        # если режим текущих котировок:
        # - получим starttime = максимальное время из БД
        # - таблицу не создаем
        # - историю не сохраняем
        if mode == 'last':
            cur.execute(f"select max(dt) from '{db_name}'")
            lastdate = cur.fetchone()[0]
            # если таблица пустая - грузим сутки
            if lastdate is None or lastdate == 0:
                lastdate = int(time.time()) - 1 * 24 * 60 * 60
            starttime = lastdate

        if mode == 'all':
            create_table(con, db_name)
            if del_history:
                # print(f"Delete data older than {nday} days")
                cur.execute(f"delete from '{db_name}' where dt < {starttime}")

        # print(f"Download symbol '{s}'")
        since = starttime
        symbol = s
        now = int(time.time())
        stop_loop = False
        while since < now:
            # если следующий блок перекрывает текущую дату - это последние данные для чтения
            endtime = since + limit * 60
            if endtime > now:
                endtime = now
                stop_loop = True

            nrate = 0
            # проверим, загруженны ли данные в таблицу за этот диапазон дат
            if mode == 'all':
                sql = f"select count(*) from '{db_name}' where (dt between {since} and {endtime}) "
                cur.execute(sql)
                nrate = cur.fetchone()[0]

            # если количество меньше limit, то эти данные загружаем
            if nrate is None or nrate <= limit:
                # print(f"nrate={nrate}")
                try:
                    if conf['exchanges'][exch].get('source', '') == 'cc':
                        orders = cc.get_price_minute(exch,symbol)
                    else:
                        # orders = exchange.fetch_ohlcv(symbol=symbol, timeframe='1m', since=since * 1000, limit=limit)
                        orders = exchange.fetch_trades(symbol=symbol, since=since * 1000, limit=limit)
                except Exception as e:
                    print(f"Error: {e}. Exit...")
                    break

                # изменим время с мс на с
                # for i in orders:
                #     i['timestamp'] = int(i['timestamp'] / 1000)

                if len(orders) == 0:
                    print(f"Error: Get NULL responce from '{exch}:{s}'. Exit...")
                    break
                t1 = utc2s(orders[0][0])
                t2 = utc2s(orders[len(orders) - 1][0])
                print(f"{exch}:{s}: [{t1}] - [{t2}] save")
                # сохранить котировки в БД
                for i in orders:
                    cur.execute(
                        f"insert or ignore into '{db_name}' values "
                        f"('{int(i[0])}','{i[1]}','{i[2]}','{i[3]}','{i[4]}','{i[5]}')")
                con.commit()

                if stop_loop:
                    break

                if len(orders):
                    # Если разница меньше 60 сек - выход
                    # if orders[len(orders) - 1][0] - since < 60 * 1000:
                    #     break
                    since = orders[len(orders) - 1][0]
                else:
                    break
            # если количество данных равно limit, то эти данные загруженны, пропускаем
            else:
                t1 = utc2s(since)
                t2 = utc2s(endtime)
                print(f"{exch}:{s}: [{t1}] - [{t2}] pass")
                since = endtime

    if cur:
        cur.close()


def utc2s(dt):
    return datetime.fromtimestamp(dt).strftime('%Y-%m-%d %H:%M:%S')


def create_table(con, db_name):
    # print(f"Try create table '{db_name}'")
    cur = con.cursor()
    cur.execute(f"""
                CREATE TABLE IF NOT EXISTS '{db_name}'
                (dt INTEGER NOT NULL,  price REAL, amount REAL, cost REAL,
                 PRIMARY KEY (dt)
                 )
                """)
    con.commit()
    cur.close()


def create_exchange(exchange_id):
    exchange_class = getattr(ccxt, exchange_id)
    exchange = exchange_class()
    # if (exchange_id == 'cex'):
    #     exchange.options['fetchOHLCVWarning'] = False
    exchange.enableRateLimit = True
    exchange.load_markets()
    return exchange


def main():
    load_history()


if __name__ == "__main__":
    main()
