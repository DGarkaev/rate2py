#!/usr/bin/env python3

# загрузка котировок
from threading import Thread
import os, sys
import ccxt
import time
from signal import signal, SIGINT
from sys import exit

from rate import e_cccagg, e_cryptolab, e_forex
import logging
import math
from utils import common
import exmo

os.chdir(sys.path[0])

global work
work = True

# задаём уровень логгирования
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
# создаём объект с именем модуля
log = logging.getLogger('loadtrades')


# formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')


def create_table(db_name):
    """Создать таблицу, если она не существует"""
    with common.get_con() as con:
        cur = con.cursor()
        cur.execute("PRAGMA  journal_mode=WAL")
        cur.execute(f"""
                CREATE TABLE IF NOT EXISTS '{db_name}'
                (dt INTEGER NOT NULL,  
                 price REAL, 
                 amount REAL, 
                 cost REAL,
                 side TEXT,
                 PRIMARY KEY (dt, price)
                 )
                """)
        con.commit()
        cur.close()


def load_trades(e):
    global work

    log.info(f"thread '{e}' start")

    conf = common.get_conf()
    pairs = conf['exchanges'][e]['pairs']
    ratelimit = conf['exchanges'][e].get('ratelimit')
    interval = conf['exchanges'][e].get('interval')

    exch = get_exchange(e)
    if exch is None:
        return

    con = common.get_con()
    cur = con.cursor()
    cur.execute("PRAGMA  journal_mode=WAL")
    while work:

        # если задан интервал - спим до ближайшей границы интервала
        if interval is not None:
            sec = time.time()  # datetime.datetime.now().second
            # вычислим, сколько секунд до ближайшего интервала
            ds = math.ceil(sec / interval) * interval - sec
            # ожидаем
            time.sleep(ds)

        # Получить сделки по валюте
        # Проходим по списку валютных пар
        for s in list(pairs):
            tbl_name = get_tblname(e, s)
            # Пока не будем брать последнюю дату из БД, т.к. грабли
            # last_time = get_last_time(con, tbl_name)
            try:
                # получаем сделки с биржи
                # trades = exch.fetch_trades(symbol=s, since=last_time)
                trades = exch.fetch_trades(symbol=s)
                # Если массив сделок пришел пустой - удаляем этот элемент из списка
                # if len(trades) == 0:
                #     print(f"Warning: {tbl_name} trades is Null")
                #     i = pairs.index(s)
                #     del pairs[i]
                #     continue
            except Exception as ex:
                log.error(f"[{tbl_name}]:{ex}")
                continue

            # сохранить котировки в БД
            val = []
            for t in trades:
                dt = t["timestamp"]
                price = t["price"]
                amount = 0.0 if t["amount"] is None else t["amount"]
                cost = 0.0 if t["cost"] is None else t["cost"]
                # side = 'none' if t["side"] is None else t["side"]
                side = t["side"]

                val.append((dt, price, amount, cost, side))

            # sql = f"""replace into '{tbl_name}' values(?, ?, ?, ?, ?)"""
            sql = f"""insert or ignore into '{tbl_name}' values(?, ?, ?, ?, ?)"""
            try:
                rz = cur.executemany(sql, val)
                if rz.rowcount != 0:
                    log.info(f"{rz.rowcount} rows add to '{tbl_name}'")
            except Exception as te:
                log.error(te)
                con.rollback()
                continue
            con.commit()
            # print(f"{tbl_name}:last_time={mutc2s(last_time)}")

    cur.close()
    if con:
        con.close()
    log.info(f"thread '{e}' stop")


# def mutc2s(dt):
#     d = dt / 1000
#     return datetime.utcfromtimestamp(d).strftime('[UTC]:%Y-%m-%d %H:%M:%S')


def get_last_time(con, db_name):
    cur = con.cursor()
    sql = f"select max(dt) from '{db_name}'"
    cur.execute(sql)
    last_time = cur.fetchone()[0]
    if last_time is None:
        last_time = (int(time.time()) - 7 * 24 * 60 * 60) * 1000
    return last_time


def get_exchange(exchange_id):
    try:
        if exchange_id == 'forex':
            return e_forex.forex()

        if exchange_id == 'cccagg':
            return None
            # return cccagg.cccagg()

        if exchange_id == 'cryptolab':
            return e_cryptolab.cryptolab()

        exchange_class = getattr(ccxt, exchange_id)
        exchange = exchange_class()
        exchange.enableRateLimit = True
        exchange.load_markets()
        return exchange
    except Exception as e:
        log.error(e)
    return None


def init():
    """Инициализация работы"""

    # Создадим все таблицы
    conf = common.get_conf()
    for e in conf['exchanges']:
        for s in conf['exchanges'][e]['pairs']:
            db_name = get_tblname(e, s)
            create_table(db_name)

    # Запустим на каждую биржу поток
    threads = []
    for e in conf['exchanges']:
        th = None
        if e == 'exmo':
            exch = exmo.Exmo()
            th = (Thread(target=exch.run, name=e))

        if e == 'cccagg':
            exch = e_cccagg.Cccagg()
            th = (Thread(target=exch.run, name=e))

        if th is None:
            th = (Thread(target=load_trades, name=e, args=(e,)))

        threads.append(th)

    for t in threads:
        t.start()

    for t in threads:
        t.join()


def get_tblname(e, s):
    tbl_name = f"{e}::{s}"
    return tbl_name


def handlerSIGINT(signal_received, frame):
    global work
    work = False
    # Handle any cleanup here
    log.info('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)


def main():
    signal(SIGINT, handlerSIGINT)
    init()


if __name__ == "__main__":
    main()
    log.info("Finish")
